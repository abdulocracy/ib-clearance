To-Do List
==========

## v0.2
- [x] Error handling (when entering invalid data types)
- [x] Make database contain data for all classes
- [x] Flexible mandatory subjects
- [x] Make script functional (sorta)
- [x] Document the damn thing

## v0.3
- [ ] Usability and GUI
- [ ] Timetable editor (add & remove lessons, view timetable)
