# ib-clearance

ib-clearance is a program which helps schools manage the entrance and exiting of their IB Diploma Program students. Due to the unique nature of IB students' timetables, the program helps to automatically check individual timetables and give clearance to students who have finished for the day.

## Building & running

The code is currently designed to run on Python 3.6.* and requires the [`getch`](https://pypi.org/project/getch/) module on platforms other than Windows.

Binaries can be built using [`pyinstaller`](https://pypi.org/project/PyInstaller/): `pyinstaller --onefile ib-clearance.py`

## Database specification

The program requires a SQLite database `database.db` in the working directory, containing data for all school classes.

`database.db` must contain:

| Table       | Description                                                  |
| ----------- | ------------------------------------------------------------ |
| `students`  | Contains students' names, ID numbers and subject preferences |
| `timetable` | Timetable information (lessons)                              |

### `students`

| Column          | Data type | Description                                     |
| --------------- | --------- | ----------------------------------------------- |
| `id`            | `INTEGER` | Student ID number used in program               |
| `student_class` | `INTEGER` | IB class number (e.g. `1`, `2`)                 |
| `name`          | `TEXT`    | Full name of student                            |
| `g1`            | `TEXT`    | Group 1 subject ID                              |
| `g2`            | `TEXT`    | Group 2 subject ID                              |
| `g3`            | `TEXT`    | Group 3 subject ID                              |
| `g4`            | `TEXT`    | Group 4 subject ID                              |
| `g5`            | `TEXT`    | Group 5 subject ID                              |
| `g6`            | `TEXT`    | Group 6 subject ID                              |
| `other`         | `TEXT`    | ID(s) of other class(es) taken, space seperated |

### `timetable`

| Column         | Data type | Description                                   |
| -------------- | --------- | --------------------------------------------- |
| `lesson_class` | `INTEGER` | IB class which takes lesson (e.g. `1`)        |
| `day`          | `TEXT`    | Day of the week (e.g. `monday`)               |
| `end_time`     | `TEXT`    | Ending time of lesson (e.g. `09:55`, `13:05`) |
| `lesson_id`    | `TEXT`    | Subject ID of the lesson (e.g. `tok`)         |

#### Subject IDs

Subject IDs may be anything, as long as consistency is maintained throughout the tables.

## License

ib-clearance is made available under the Apache License 2.0.

Copyright 2018-2019 Abdulkadir Furkan Şanlı

Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved.  This file is offered as-is, without any warranty.
